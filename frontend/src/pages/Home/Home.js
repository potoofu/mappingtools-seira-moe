import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./Home.css";

class Home extends Component {
    render() {
        return (
            <div className="home-grid">
                <div class="main-image"></div>
                <h1 class="main-text display-4">Get started with Mapping Tools for osu!</h1>
                <section class="features container-fluid">
                    <div class="row">
                        <div class="col-mg-6 col-lg-3">
                            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                                <div class="features-icons-icon d-flex">
                                    <FontAwesomeIcon className="feature-icon" icon={["fa", "broom"]}></FontAwesomeIcon>
                                </div>
                                <h3>Clean Up Your Maps</h3>
                                <p class="lead mb-0">Remove all unneeded greenlines, resnap greenlines and objects automatically. Simplifying and minifying your maps will save you storage and get you around ranking criterias. It also makes sharing your creation with friends easier.</p>
                            </div>
                        </div>
                        <div class="col-mg-6 col-lg-3">
                            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                                <div class="features-icons-icon d-flex">
                                    <FontAwesomeIcon className="feature-icon" icon={["fa", "music"]}></FontAwesomeIcon>
                                </div>
                                <h3>Diversify Your Hitsounding</h3>
                                <p class="lead mb-0">Use tools like the Hitsound Studio or the Hitsound Copier to create unique and complex hitsounding that is guaranteed to work and easy to modify.</p>
                            </div>
                        </div>
                        <div class="col-mg-6 col-lg-3">
                            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                                <div class="features-icons-icon d-flex">
                                    <FontAwesomeIcon className="feature-icon" icon={["fa", "expand-arrows-alt"]}></FontAwesomeIcon>
                                </div>
                                <h3>Unleash Your Creativity</h3>
                                <p class="lead mb-0">The sky is the limit with Sliderator, Slider Completionator and many other tools that let you use a user interface to manipulate objects instead of being limited by learning how to manipulate the .osu code.</p>
                            </div>
                        </div>
                        <div class="col-mg-6 col-lg-3">
                            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                                <div class="features-icons-icon d-flex">
                                    <FontAwesomeIcon className="feature-icon" icon={["fa", "clock"]}></FontAwesomeIcon>
                                </div>
                                <h3>Easy To Learn</h3>
                                <p class="lead mb-0">Every tool has an explanation about its use and what you can specifically do with it directly in the app.
                                Additionally you can always ask questions on the <a href="https://discord.gg/JhP964H"> community discord</a> and watch guides on the
                                <a href="https://www.youtube.com/playlist?list=PLuijZnrwVA86pO7zIP9oVu-7YAQ5qEe2Y"> youtube channel</a>.
                            </p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default Home;