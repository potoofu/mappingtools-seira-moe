import React, {Component} from "react";
import "./FAQ.css";
import ReactHtmlParser from 'react-html-parser';
import { Card } from "react-bootstrap";

class FAQ extends Component{
    constructor() {
        super();
        this.state = {
            data: null
        }
    }

    async componentDidMount() {
        const res = await fetch("https://mappingtools.seira.moe/faq/faqs", {
            method: "POST"
        });
        const data = await res.json();
        this.setState((state) => {
            return { data: data }
        });
    }

    render(){
        return (
            <div class="faq-container">
                {this.state.data ? 
                    this.state.data.map(item => {
                        const html_text = item.answer;
                        return (
                            <Card bg="primary" text="white" className="faq-card">
                                <Card.Header className="faq-card-header">
                                    <span>Q: {item.question}</span>
                                </Card.Header>
                                <Card.Body className="bg-dark faq-card-body">
                                    <p>{ReactHtmlParser(html_text)}</p>
                                </Card.Body>
                            </Card>
                        );
                    })

                    :

                    <h2>Loading...</h2>
                }
            </div>
        );
    }
}

export default FAQ;