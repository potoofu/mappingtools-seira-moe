const fs = require("fs");
const path = require("path");
const express = require("express");
const app = express();
const mongo = require('mongodb').MongoClient;
require('dotenv').config()

app.use(express.static(path.join(__dirname, '../data')));

app.get('*', (req, res) => {
  res.redirect("https://mappingtools.github.io")
});

app.post("/download/win/installer", (req, res) => {
  res.setHeader("content-type", "octet/binary");
  fs.createReadStream(path.join(__dirname, "../data/current/mapping-tools.exe")).pipe(res);
});

app.post("/download/win/archive", (req, res) => {
  res.setHeader("content-type", "octet/binary");
  fs.createReadStream(path.join(__dirname, "../data/current/mapping-tools.zip")).pipe(res);
});

app.listen(process.env.PORT);
